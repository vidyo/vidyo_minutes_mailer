#!/usr/bin/env python

"""
Fetch Vidyo usage data from last months, send to admins by email.

Usage data is the sum of minutes of all users per Vidyo meeting per month in all
meetings with more than 1 participants

Requirements:
- MySQL-python (https://pypi.python.org/pypi/MySQL-python/1.2.5)

Aleksi Pekkala (aleksi.v.a.pekkala@student.jyu.fi)
12.5.2014
"""

from datetime import datetime
import MySQLdb
from email.mime.text import MIMEText
from subprocess import Popen, PIPE
from collections import defaultdict


from conf import VIDYO_DBCDR, MAIL_RECEIVERS, MONTHS, MAIL_SENDER


def main():
    now = datetime.now()
    if now.month > MONTHS:
        start_date = datetime(now.year, now.month - MONTHS, 1)
    else:
        start_date = datetime(now.year - 1, now.month + MONTHS, 1)

    months = defaultdict(int)
    for i in range(MONTHS):
        if start_date.month == 12:
            end_date = datetime(start_date.year + 1, 1, 1)
        else:
            end_date = datetime(start_date.year, start_date.month + 1, 1)

        meetings = get_meetings(start_date, end_date)
        key = start_date.strftime('%Y-%m')
        months[key] = sum(m['seconds'] for m in meetings.itervalues()) / 60

        start_date = end_date

    output = ['%s - %s minutes (%s days)' % (k, months[k], months[k]/1440) for k in sorted(months)]

    msg = (
        'Monthly Vidyo usage minutes*:\n'
        '\n%s\n'
        '\n*The sum of minutes of all users per Vidyo meeting in all meetings with more than 1 participants'
    ) % '\n'.join(output)

    for receiver in MAIL_RECEIVERS:
        sendmail(
            sender=MAIL_SENDER,
            receiver=receiver,
            subject='Vidyo usage stats',
            content=msg
        )


def get_meetings(start_date, end_date):
    """
    Fetch all meetings with more than 1 participants in given date range.

    :param start_date: inclusive start date
    :param end_date: exclusive end date
    """
    query = """SELECT UniqueCallID, ConferenceName, CallerName, JoinTime, LeaveTime
        FROM ConferenceCall2
        WHERE CallState='COMPLETED'
        AND LeaveTime > %s
        AND JoinTime < %s
        ORDER BY JoinTime ASC;
    """

    db = MySQLdb.connect(**VIDYO_DBCDR)
    cur = db.cursor()
    cur.execute(query, (start_date, end_date))

    meetings = dict()
    for call_id, conf_name, caller_name, join_time, leave_time in cur.fetchall():

        if not call_id in meetings:
            meetings[call_id] = {'participants': set(), 'seconds': 0, 'date': join_time}

        try:
            dur = min(leave_time, end_date) - max(join_time, start_date)
        except TypeError:
            continue  # dates can sometimes be null

        meetings[call_id]['seconds'] += (dur.seconds + dur.days * 24 * 3600)
        meetings[call_id]['date'] = min(meetings[call_id]['date'], join_time)
        meetings[call_id]['participants'].add(caller_name)

    cur.close()

    filtered_meetings = dict([(k, v) for k, v in meetings.iteritems() if len(v['participants']) > 1])
    return filtered_meetings


def sendmail(sender, receiver, subject, content):
    msg = MIMEText(content)
    msg['From'] = sender
    msg['To'] = receiver
    msg['Subject'] = 'Vidyo monthly usage stats'
    p = Popen(['/usr/sbin/sendmail', '-t'], stdin=PIPE)
    p.communicate(msg.as_string())


if __name__ == '__main__':
    main()

